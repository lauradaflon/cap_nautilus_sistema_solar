# Guide of use #

**How do I run the program?**
First, you need to have on your computer the [ROS](http://wiki.ros.org/) aplication. After having it, you have to type `roscore` in your prompt to run the Master and then launch the program in another prompt with `roslaunch`. Then, you can run RViz just by typing `rviz` in the prompt.

**What I can do about the solar reference?**
You can change the reference to any planet listed, just by selecting it in the program.

**How the system works?**
As it's just for reference, the values are aproximated and some were adapted to the model.

